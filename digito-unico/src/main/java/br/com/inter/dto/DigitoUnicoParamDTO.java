package br.com.inter.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DigitoUnicoParamDTO {

	private Integer idUsuario;
	private Integer digito;
	private Integer qtdConcatenacao;
}