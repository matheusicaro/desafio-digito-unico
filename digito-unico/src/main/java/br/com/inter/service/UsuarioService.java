package br.com.inter.service;

import static br.com.inter.comum.CriptografiaUtils.isChavesValida;
import static br.com.inter.comum.ValidacaoUtils.throwIf;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.Base64;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.inter.comum.CriptografiaUtils;
import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;
import br.com.inter.repository.DigitoUnicoRepository;
import br.com.inter.repository.UsuarioRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class UsuarioService {

	@NonNull
	private UsuarioRepository repository;
	@NonNull
	private DigitoUnicoRepository digitoUnicoRepository;

	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return this.repository.findAll();
	}

	@Transactional(readOnly = true)
	public Usuario findById(Integer id) {
		return this.repository.findById(id).orElse(null);
	}

	@Transactional
	public void deleteById(Integer id) {
		this.repository.deleteById(id);
	}

	@Transactional
	public Usuario save(Usuario usuario) {
		this.validarUsuario(usuario);
		return this.repository.save(usuario);
	}

	@Transactional(readOnly = true)
	public boolean existUsuario(Integer id) {
		throwIf(id, Objects::isNull, "ID usuário não pode ser nulo.");
		return this.repository.existUsuario(id);
	}

	@Transactional(readOnly = true)
	public List<DigitoUnico> findAllDigitoUnico(Integer id) {
		throwIf(id, Objects::isNull, "ID usuário não pode ser nulo.");
		return this.digitoUnicoRepository.findAllByIdUsuario(id);
	}

	@Transactional
	public void criptografar(Integer id, String chavePublica, String chavePrivada) {
		throwIf(id, Objects::isNull, "ID usuário não pode ser nulo.");
		throwIf(isChavesValida(chavePublica, chavePrivada), Boolean.FALSE::equals, "As chaves informadas não são chaves par. Chave pública ou privada são inválidas.");
		Usuario usuario = this.repository.findById(id).orElse(null);
		if (usuario != null) {
			byte[] nomeCriptografado = CriptografiaUtils.criptografar(usuario.getNome(), chavePublica);
			byte[] emailCriptografado = CriptografiaUtils.criptografar(usuario.getEmail(), chavePublica);
			Base64.Encoder encoder = Base64.getEncoder();
			usuario.setNome(encoder.encodeToString(nomeCriptografado));
			usuario.setEmail(encoder.encodeToString(emailCriptografado));
			usuario.setChavePublicaCriptografia(chavePublica);
			usuario.setChavePrivadaCriptografia(chavePrivada);
			this.repository.save(usuario);
		}
	}

	@Transactional(readOnly = true)
	public Usuario descriptografar(Integer id, String chavePrivada) {
		throwIf(id, Objects::isNull, "ID usuário não pode ser nulo.");
		throwIf(chavePrivada, StringUtils::isEmpty, "Chave privada não pode ser nula ou vazia.");
		Usuario usuario = this.repository.findById(id).orElse(null);
		if (usuario != null && isNotEmpty(usuario.getChavePublicaCriptografia())) {
			throwIf(isChavesValida(usuario.getChavePublicaCriptografia(), chavePrivada), Boolean.FALSE::equals, "A chave privada é inválida.");
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] nomeDecode = decoder.decode(usuario.getNome());
			String nomeDescriptografado = CriptografiaUtils.descriptografar(nomeDecode, chavePrivada);
			usuario.setNome(nomeDescriptografado);
			byte[] emailDecode = decoder.decode(usuario.getEmail());
			String emailDescriptografado = CriptografiaUtils.descriptografar(emailDecode, chavePrivada);
			usuario.setEmail(emailDescriptografado);
		}
		return usuario;
	}

	private void validarUsuario(Usuario usuario) {
		throwIf(usuario, Objects::isNull, "Usuário não pode ser nulo.");
		throwIf(usuario.getNome(), StringUtils::isEmpty, "Nome não pode ser nulo ou vazio.");
		throwIf(usuario.getEmail(), StringUtils::isEmpty, "E-mail não pode ser nulo ou vazio.");
	}
}