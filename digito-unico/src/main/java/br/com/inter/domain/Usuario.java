package br.com.inter.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Usuario extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id_usuario", nullable = false)
	private Integer id;

	@Column(name = "nome", nullable = false, length = 4000)
	private String nome;

	@Column(name = "email", nullable = false, length = 4000)
	private String email;

	@Column(name = "chave_publica_criptografia", nullable = true, length = 4000)
	private String chavePublicaCriptografia;

	@Column(name = "chave_privada_criptografia", nullable = true, length = 4000)
	private String chavePrivadaCriptografia;

	@OneToMany(mappedBy = "usuario", targetEntity = DigitoUnico.class, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<DigitoUnico> digitoUnicos = new HashSet<>();
}