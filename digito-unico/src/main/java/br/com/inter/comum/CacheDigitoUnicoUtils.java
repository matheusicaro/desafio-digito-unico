package br.com.inter.comum;

import java.util.LinkedHashMap;

public class CacheDigitoUnicoUtils {

	private static LinkedHashMap<String, Integer> CACHE_DIGITO_UNICO = new LinkedHashMap<>();

	public static void adicionarCache(String digito, Integer digitoUnico) {
		adicionarCache(digito, null, digitoUnico);
	}

	public static void adicionarCache(String digito, Integer qtdConcatenacao, Integer digitoUnico) {
		if (CACHE_DIGITO_UNICO.size() == 10) {
			String firstKey = CACHE_DIGITO_UNICO.keySet().stream().findFirst().get();
			CACHE_DIGITO_UNICO.remove(firstKey);
		}
		String key = gerarKey(digito, qtdConcatenacao);
		if (!CACHE_DIGITO_UNICO.containsKey(key)) {
			CACHE_DIGITO_UNICO.put(key, digitoUnico);
		}
	}

	public static Integer getDigitoUnicoCache(String digito, Integer qtdConcatenacao) {
		Integer digitoUnico = null;
		String key = gerarKey(digito, qtdConcatenacao);
		if (CACHE_DIGITO_UNICO.containsKey(key)) {
			digitoUnico = CACHE_DIGITO_UNICO.get(key);
		}
		return digitoUnico;
	}

	public static Integer sizeCache() {
		return CACHE_DIGITO_UNICO.size();
	}

	public static void clearCache() {
		CACHE_DIGITO_UNICO.clear();
	}

	private static String gerarKey(String digito, Integer qtdConcatenacao) {
		String keyQtdConcatenacao = (qtdConcatenacao == null) ? "NA" : qtdConcatenacao.toString();
		return "|" + digito + "|" + keyQtdConcatenacao + "|";
	}
}