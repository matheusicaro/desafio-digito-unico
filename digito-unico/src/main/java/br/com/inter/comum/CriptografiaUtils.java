package br.com.inter.comum;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

public class CriptografiaUtils {

	private static final String ALGORITMO = "RSA";
	private static final Integer KEY_SIZE = 2048;
	private static final Base64.Encoder ENCODER = Base64.getEncoder();

	public static ChavesCriptografia gerarChaves() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITMO);
			keyGen.initialize(KEY_SIZE);
			KeyPair keyPair = keyGen.generateKeyPair();
			String chavePublica = ENCODER.encodeToString(keyPair.getPublic().getEncoded());
			String chavePrivada = ENCODER.encodeToString(keyPair.getPrivate().getEncoded());
			return ChavesCriptografia.builder().chavePublica(chavePublica).chavePrivada(chavePrivada).build();
		} catch (Exception e) {
			throw new ServiceException("Ocorreu um erro ao gerar as chaves de criptografia.");
		}
	}

	public static byte[] criptografar(String texto, String chavePublica) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITMO);
			PublicKey publicKey = getPublicKey(chavePublica);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return cipher.doFinal(texto.getBytes());
		} catch (Exception e) {
			throw new ServiceException("Ocorreu um erro ao criptografar.");
		}
	}

	public static String descriptografar(byte[] texto, String chavePrivada) {
		try {
			final Cipher cipher = Cipher.getInstance(ALGORITMO);
			PrivateKey privateKey = getPrivateKey(chavePrivada);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return new String(cipher.doFinal(texto));
		} catch (Exception e) {
			throw new ServiceException("Ocorreu um erro ao descriptografar.");
		}
	}

	public static boolean isChavesValida(String chavePublica, String chavePrivada) {
		try {
			String textoBase = "texto base";
			byte[] textoCriptografado = criptografar(textoBase, chavePublica);
			String textoDescriptografado = descriptografar(textoCriptografado, chavePrivada);
			return textoBase.equals(textoDescriptografado);
		} catch (Exception e) {
			return false;
		}
	}

	private static PublicKey getPublicKey(String chavePublicaBase64) {
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(chavePublicaBase64.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITMO);
			return keyFactory.generatePublic(keySpec);
		} catch (Exception e) {
			throw new ServiceException("Chave pública inválida.");
		}
	}

	private static PrivateKey getPrivateKey(String chavePrivadaBase64) {
		try {
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(chavePrivadaBase64.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITMO);
			return keyFactory.generatePrivate(keySpec);
		} catch (Exception e) {
			throw new ServiceException("Chave privada inválida.");
		}

	}
}