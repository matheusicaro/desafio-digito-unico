package br.com.inter.comum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class DigitoUnicoUtils {

	public static BigInteger DIGITO_MAX = BigInteger.TEN.pow(1000000);
	public static BigInteger CONCATENACAO_MAX = BigInteger.TEN.pow(5);
	private static BigInteger MAIOR_DIGITO_UNICO = new BigInteger("9");

	public static Integer calcularDigitoUnico(String digito, Integer qtdConcatenacao) {
		if (StringUtils.isEmpty(digito) || qtdConcatenacao == null) {
			return null;
		}
		validarDigito(digito);
		validarQtdConcatenacao(qtdConcatenacao);
		String digitoConcatenado = String.valueOf("");
		for (int i = 0; i < qtdConcatenacao; i++) {
			digitoConcatenado = digitoConcatenado.concat(digito);
		}
		return digitoUnico(new BigInteger(digitoConcatenado));
	}

	public static Integer calcularDigitoUnico(Integer digito) {
		if (StringUtils.isEmpty(digito.toString())) {
			return null;
		}
		validarDigito(digito.toString());
		return digitoUnico(BigInteger.valueOf(digito));
	}

	private static Integer digitoUnico(BigInteger digito) {
		if (digito == null) {
			return null;
		}
		List<BigInteger> algarismos = parseToAlgarismos(digito);
		BigInteger result = algarismos.stream().reduce(BigInteger.ZERO, (subtotal, element) -> subtotal.add(element));
		if (MAIOR_DIGITO_UNICO.compareTo(result) < 0) {
			return digitoUnico(result);
		}
		return result.intValue();
	}

	private static List<BigInteger> parseToAlgarismos(BigInteger digito) {
		List<BigInteger> algarismos = new ArrayList<>();
		char[] characters = digito.toString().toCharArray();
		for (char character : characters) {
			String algarismo = String.valueOf(character);
			algarismos.add(new BigInteger(algarismo));
		}
		return algarismos;
	}

	private static void validarDigito(String digito) {
		BigInteger digitoInt = null;
		try {
			digitoInt = new BigInteger(digito);
		} catch (NumberFormatException | NullPointerException nfe) {
			throw new ServiceException("Dígito inválido.");
		}
		if ((digitoInt.compareTo(BigInteger.ONE) < 0) || (DIGITO_MAX.compareTo(digitoInt) < 0)) {
			throw new ServiceException("Dígito inválido.");
		}
	}

	private static void validarQtdConcatenacao(Integer qtdConcatenacao) {
		if (qtdConcatenacao < 1 || (CONCATENACAO_MAX.compareTo(BigInteger.valueOf(qtdConcatenacao)) < 0)) {
			throw new ServiceException("Quantidade de concatenação inválido.");
		}
	}
}