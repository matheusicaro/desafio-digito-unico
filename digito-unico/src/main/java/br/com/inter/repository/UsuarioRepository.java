package br.com.inter.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Override
	@Query("SELECT DISTINCT u FROM Usuario u LEFT JOIN FETCH u.digitoUnicos")
	public List<Usuario> findAll();

	@Override
	@Query("SELECT DISTINCT u FROM Usuario u LEFT JOIN FETCH u.digitoUnicos WHERE u.id = :id")
	public Optional<Usuario> findById(@Param("id") Integer id);

	@Query("SELECT CASE WHEN COUNT(*) > 0 THEN true ELSE false END FROM Usuario u WHERE u.id = :id")
	public boolean existUsuario(@Param("id") Integer id);

	@Query("SELECT DISTINCT du FROM Usuario u INNER JOIN u.digitoUnicos du WHERE u.id = :id AND du.digito = :digito AND du.qtdConcatenacao = :qtdConcatenacao")
	public Optional<DigitoUnico> findDigitoUnico(@Param("id") Integer id, @Param("digito") String digito, @Param("qtdConcatenacao") Integer qtdConcatenacao);
}