package br.com.inter.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.as.DigitoUnicoAS;
import br.com.inter.dto.DigitoUnicoParamDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/digito-unico")
public class DigitoUnicoAPI {

	@NonNull
	private DigitoUnicoAS as;

	@PostMapping("/calcular")
	public Integer calcular(@RequestBody DigitoUnicoParamDTO digitoUnicoParam) {
		return this.as.calcular(digitoUnicoParam);
	}
}