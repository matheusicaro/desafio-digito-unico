package br.com.inter.service.test;

import static br.com.inter.comum.CriptografiaUtils.criptografar;
import static br.com.inter.comum.CriptografiaUtils.descriptografar;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import java.util.Base64;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.comum.ChavesCriptografia;
import br.com.inter.comum.CriptografiaUtils;
import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;
import br.com.inter.repository.DigitoUnicoRepository;
import br.com.inter.repository.UsuarioRepository;
import br.com.inter.service.UsuarioService;

@RunWith(NestedRunner.class)
@SpringBootTest
public class UsuarioServiceTest {

	private static final int ID = 1;
	private Usuario usuario;

	private UsuarioService service;
	@Mock
	private UsuarioRepository repository;
	@Mock
	private DigitoUnicoRepository digitoUnicoRepository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.service = new UsuarioService(this.repository, this.digitoUnicoRepository);
	}

	public class FindAll extends UsuarioServiceTest {

		@Override
		@Before
		public void setUp() {
			super.setUp();
			super.usuario = Usuario.builder().id(ID).build();
			doAnswer(e -> asList(super.usuario)).when(super.repository).findAll();
		}

		@Test
		public void chama_repository() {
			super.service.findAll();
			verify(super.repository).findAll();
		}

		@Test
		public void retorna_list() throws Exception {
			assertThat(super.service.findAll()).isEqualTo(asList(super.usuario));
		}
	}

	public class FindById extends UsuarioServiceTest {

		@Override
		@Before
		public void setUp() {
			super.setUp();
			super.usuario = Usuario.builder().id(ID).build();
			doAnswer(e -> Optional.of(super.usuario)).when(super.repository).findById(ID);
		}

		@Test
		public void chama_repository() {
			super.service.findById(ID);
			verify(super.repository).findById(ID);
		}

		@Test
		public void retorna_usuario() throws Exception {
			assertThat(super.service.findById(ID)).isEqualTo(super.usuario);
		}
	}

	public class DeleteById extends UsuarioServiceTest {

		@Test
		public void chama_repository() {
			super.service.deleteById(ID);
			verify(super.repository).deleteById(ID);
		}
	}

	public class Save extends UsuarioServiceTest {

		@Captor
		private ArgumentCaptor<Usuario> usuarioCaptor;

		@Override
		public void setUp() {
			super.setUp();
			super.usuario = Usuario.builder().nome("Nome").email("nome@email.com").build();
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.service.save(null))).hasMessageContaining("Usuário não pode ser nulo.");
			super.usuario.setNome(null);
			assertThat(catchThrowable(() -> super.service.save(super.usuario))).hasMessageContaining("Nome não pode ser nulo ou vazio.");
			super.usuario.setNome("Nome");
			super.usuario.setEmail(null);
			assertThat(catchThrowable(() -> super.service.save(super.usuario))).hasMessageContaining("E-mail não pode ser nulo ou vazio.");
		}

		@Test
		public void incluir() {
			super.service.save(super.usuario);
			verify(super.repository).save(this.usuarioCaptor.capture());
			assertThat(this.usuarioCaptor.getValue().getId()).isNull();
			assertThat(this.usuarioCaptor.getValue().getNome()).isEqualTo(super.usuario.getNome());
			assertThat(this.usuarioCaptor.getValue().getEmail()).isEqualTo(super.usuario.getEmail());
		}

		@Test
		public void alterar() {
			super.usuario.setId(ID);
			super.service.save(super.usuario);
			verify(super.repository).save(this.usuarioCaptor.capture());
			assertThat(this.usuarioCaptor.getValue().getId()).isEqualTo(ID);
			assertThat(this.usuarioCaptor.getValue().getNome()).isEqualTo(super.usuario.getNome());
			assertThat(this.usuarioCaptor.getValue().getEmail()).isEqualTo(super.usuario.getEmail());
		}
	}

	public class ExistUsuarios extends UsuarioServiceTest {

		@Override
		public void setUp() {
			super.setUp();
			doAnswer(e -> true).when(super.repository).existUsuario(ID);
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.service.existUsuario(null))).hasMessageContaining("ID usuário não pode ser nulo.");
		}

		@Test
		public void existUsuarios() {
			assertThat(super.service.existUsuario(ID)).isTrue();
			assertThat(super.service.existUsuario(2)).isFalse();
		}
	}

	public class FindAllDigitoUnico extends UsuarioServiceTest {

		private DigitoUnico digitoUnico;

		@Override
		@Before
		public void setUp() {
			super.setUp();
			this.digitoUnico = DigitoUnico.builder().id(ID).build();
			doAnswer(e -> asList(this.digitoUnico)).when(super.digitoUnicoRepository).findAllByIdUsuario(ID);
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.service.findAllDigitoUnico(null))).hasMessageContaining("ID usuário não pode ser nulo.");
		}

		@Test
		public void chama_repository() {
			super.service.findAllDigitoUnico(ID);
			verify(super.digitoUnicoRepository).findAllByIdUsuario(ID);
		}

		@Test
		public void retorna_list() throws Exception {
			assertThat(super.service.findAllDigitoUnico(ID)).isEqualTo(asList(this.digitoUnico));
		}
	}

	public class Criptografar extends UsuarioServiceTest {

		@Captor
		private ArgumentCaptor<Usuario> usuarioCaptor;
		private ChavesCriptografia chavesCriptografia;
		private Base64.Decoder decoder = Base64.getDecoder();

		@Override
		@Before
		public void setUp() {
			super.setUp();
			this.chavesCriptografia = CriptografiaUtils.gerarChaves();
			super.usuario = Usuario.builder().id(ID).nome("Nome").email("nome@email.com").build();
			doAnswer(e -> Optional.of(super.usuario)).when(super.repository).findById(ID);
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.service.criptografar(null, null, null))).hasMessageContaining("ID usuário não pode ser nulo.");
		}

		@Test
		public void criptografia() throws Exception {
			super.service.criptografar(ID, this.chavesCriptografia.getChavePublica(), this.chavesCriptografia.getChavePrivada());
			verify(super.repository).save(this.usuarioCaptor.capture());
			assertThat(this.usuarioCaptor.getValue().getId()).isEqualTo(ID);
			assertThat(this.usuarioCaptor.getValue().getChavePublicaCriptografia()).isNotNull();
			assertThat(this.usuarioCaptor.getValue().getChavePrivadaCriptografia()).isNotNull();
			byte[] nomeDecode = this.decoder.decode(this.usuarioCaptor.getValue().getNome());
			String nomeDescriptografado = descriptografar(nomeDecode, this.chavesCriptografia.getChavePrivada());
			assertThat("Nome").isEqualTo(nomeDescriptografado);
			byte[] emailDecode = this.decoder.decode(this.usuarioCaptor.getValue().getEmail());
			String emailDescriptografado = descriptografar(emailDecode, this.chavesCriptografia.getChavePrivada());
			assertThat("nome@email.com").isEqualTo(emailDescriptografado);
		}
	}

	public class Descriptografar extends UsuarioServiceTest {

		private ChavesCriptografia chavesCriptografia;

		@Override
		@Before
		public void setUp() {
			super.setUp();
			this.chavesCriptografia = CriptografiaUtils.gerarChaves();
			Base64.Encoder encoder = Base64.getEncoder();
			String chavePublica = this.chavesCriptografia.getChavePublica();
			byte[] nome = criptografar("Nome", chavePublica);
			byte[] email = criptografar("nome@email.com", chavePublica);
			super.usuario = Usuario.builder().id(ID).nome(encoder.encodeToString(nome)).email(encoder.encodeToString(email)).chavePublicaCriptografia(chavePublica).build();
			doAnswer(e -> Optional.of(super.usuario)).when(super.repository).findById(ID);
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.service.descriptografar(null, null))).hasMessageContaining("ID usuário não pode ser nulo.");
			assertThat(catchThrowable(() -> super.service.descriptografar(ID, null))).hasMessageContaining("Chave privada não pode ser nula ou vazia.");
			assertThat(catchThrowable(() -> super.service.descriptografar(ID, ""))).hasMessageContaining("Chave privada não pode ser nula ou vazia.");
			assertThat(catchThrowable(() -> super.service.descriptografar(ID, "teste"))).hasMessageContaining("A chave privada é inválida.");
		}

		@Test
		public void descriptografar() throws Exception {
			Usuario usuario = super.service.descriptografar(ID, this.chavesCriptografia.getChavePrivada());
			assertThat(usuario.getId()).isEqualTo(ID);
			assertThat(usuario.getNome()).isEqualTo("Nome");
			assertThat(usuario.getEmail()).isEqualTo("nome@email.com");
		}
	}
}