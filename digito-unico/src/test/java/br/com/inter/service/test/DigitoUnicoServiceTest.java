package br.com.inter.service.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;
import br.com.inter.repository.DigitoUnicoRepository;
import br.com.inter.repository.UsuarioRepository;
import br.com.inter.service.DigitoUnicoService;

@RunWith(NestedRunner.class)
@SpringBootTest
public class DigitoUnicoServiceTest {

	private static final int ID = 1;
	private static final Integer DIGITO = 9875;
	private static final Integer QTD_CONCATENACAO = 1;

	private DigitoUnicoService service;
	@Mock
	private DigitoUnicoRepository repository;
	@Mock
	private UsuarioRepository usuarioRepository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.service = new DigitoUnicoService(this.repository, this.usuarioRepository);
	}

	public class Calcular extends DigitoUnicoServiceTest {

		@Captor
		private ArgumentCaptor<DigitoUnico> digitoUnicoCaptor;

		@Override
		public void setUp() {
			super.setUp();
			doAnswer(e -> true).when(super.usuarioRepository).existUsuario(ID);
			doAnswer(e -> false).when(super.repository).existDigitoUnico(ID, DIGITO.toString(), QTD_CONCATENACAO);
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.service.calcular(ID, null))).hasMessageContaining("Digito não pode ser nulo.");
			assertThat(catchThrowable(() -> super.service.calcular(ID, null, null))).hasMessageContaining("Digito não pode ser nulo.");
		}

		@Test
		public void calcular() {
			assertThat(super.service.calcular(null, DIGITO)).isEqualTo(2);
			assertThat(super.service.calcular(null, DIGITO.toString(), QTD_CONCATENACAO)).isEqualTo(2);
		}

		@Test
		public void calcular_salvando_usuario() {
			assertThat(super.service.calcular(ID, DIGITO)).isEqualTo(2);
			verify(super.repository).save(this.digitoUnicoCaptor.capture());
			assertThat(this.digitoUnicoCaptor.getValue().getId()).isNull();
			assertThat(this.digitoUnicoCaptor.getValue().getDigito()).isEqualTo(DIGITO.toString());
			assertThat(this.digitoUnicoCaptor.getValue().getQtdConcatenacao()).isNull();
			assertThat(this.digitoUnicoCaptor.getValue().getDigitoUnico()).isEqualTo(2);
			Usuario usuario = Usuario.builder().id(ID).build();
			assertThat(this.digitoUnicoCaptor.getValue().getUsuario()).isEqualTo(usuario);
		}

		@Test
		public void calcular_salvando_usuario_com_concatenacao() {
			assertThat(super.service.calcular(ID, DIGITO.toString(), QTD_CONCATENACAO)).isEqualTo(2);
			verify(super.repository).save(this.digitoUnicoCaptor.capture());
			assertThat(this.digitoUnicoCaptor.getValue().getId()).isNull();
			assertThat(this.digitoUnicoCaptor.getValue().getDigito()).isEqualTo(DIGITO.toString());
			assertThat(this.digitoUnicoCaptor.getValue().getQtdConcatenacao()).isEqualTo(QTD_CONCATENACAO);
			assertThat(this.digitoUnicoCaptor.getValue().getDigitoUnico()).isEqualTo(2);
			Usuario usuario = Usuario.builder().id(ID).build();
			assertThat(this.digitoUnicoCaptor.getValue().getUsuario()).isEqualTo(usuario);
		}
	}
}