package br.com.inter.comum.test;

import static br.com.inter.comum.DigitoUnicoUtils.CONCATENACAO_MAX;
import static br.com.inter.comum.DigitoUnicoUtils.DIGITO_MAX;
import static br.com.inter.comum.DigitoUnicoUtils.calcularDigitoUnico;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.math.BigInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

@RunWith(NestedRunner.class)
@SpringBootTest
public class DigitoUnicoUtilsTest {

	@Test
	public void digito_unico() {
		assertThat(calcularDigitoUnico(9875)).isEqualTo(2);
		assertThat(calcularDigitoUnico(29)).isEqualTo(2);
		assertThat(calcularDigitoUnico(11)).isEqualTo(2);
		assertThat(calcularDigitoUnico(2)).isEqualTo(2);
	}

	@Test
	public void digito_unico_concatenado() {
		assertThat(calcularDigitoUnico(null, null)).isNull();
		assertThat(calcularDigitoUnico("", 1)).isNull();
		assertThat(calcularDigitoUnico("9875", null)).isNull();
		assertThat(calcularDigitoUnico("9875", 1)).isEqualTo(2);
		assertThat(calcularDigitoUnico("9875", 4)).isEqualTo(8);
		assertThat(calcularDigitoUnico("116", 1)).isEqualTo(8);
		assertThat(calcularDigitoUnico("8", 1)).isEqualTo(8);
	}

	@Test
	public void validacao_digito() {
		assertThat(catchThrowable(() -> calcularDigitoUnico("oi", 1))).hasMessageContaining("Dígito inválido.");
		assertThat(catchThrowable(() -> calcularDigitoUnico("0", 1))).hasMessageContaining("Dígito inválido.");
		String digitoMaxMaior = DIGITO_MAX.add(BigInteger.ONE).toString();
		assertThat(catchThrowable(() -> calcularDigitoUnico(digitoMaxMaior, 1))).hasMessageContaining("Dígito inválido.");
	}

	@Test
	public void validacao_qtd_concatenacao() {
		assertThat(catchThrowable(() -> calcularDigitoUnico("1", 0))).hasMessageContaining("Quantidade de concatenação inválido.");
		Integer concatenacaoMaxMaior = CONCATENACAO_MAX.add(BigInteger.ONE).intValue();
		assertThat(catchThrowable(() -> calcularDigitoUnico("1", concatenacaoMaxMaior))).hasMessageContaining("Quantidade de concatenação inválido.");
	}
}