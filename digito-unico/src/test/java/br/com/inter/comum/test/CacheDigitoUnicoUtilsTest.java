package br.com.inter.comum.test;

import static br.com.inter.comum.CacheDigitoUnicoUtils.adicionarCache;
import static br.com.inter.comum.CacheDigitoUnicoUtils.clearCache;
import static br.com.inter.comum.CacheDigitoUnicoUtils.getDigitoUnicoCache;
import static br.com.inter.comum.CacheDigitoUnicoUtils.sizeCache;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

@RunWith(NestedRunner.class)
@SpringBootTest
public class CacheDigitoUnicoUtilsTest {

	private String digito = "9875";
	private Integer qtdConcatenacao = 1;
	private Integer digitoUnico = 2;

	@Test
	public void adicionar() {
		clearCache();
		adicionarCache(this.digito, this.qtdConcatenacao, this.digitoUnico);
		assertThat(sizeCache()).isEqualTo(1);
		assertThat(getDigitoUnicoCache(this.digito, this.qtdConcatenacao)).isEqualTo(this.digitoUnico);
	}

	@Test
	public void adicionar_varios_valores_iguais() {
		clearCache();
		assertThat(sizeCache()).isEqualTo(0);
		for (int i = 0; i < 3; i++) {
			adicionarCache(this.digito, this.qtdConcatenacao, this.digitoUnico);
		}
		assertThat(sizeCache()).isEqualTo(1);
	}

	@Test
	public void get_digito_unico() {
		clearCache();
		assertThat(getDigitoUnicoCache(this.digito, this.qtdConcatenacao)).isNull();
		adicionarCache(this.digito, this.qtdConcatenacao, this.digitoUnico);
		assertThat(getDigitoUnicoCache(this.digito, this.qtdConcatenacao)).isEqualTo(this.digitoUnico);
	}

	@Test
	public void valida_apenas_10_ultimos_em_memoria() {
		clearCache();
		for (int i = 0; i < 30; i++) {
			adicionarCache(this.digito, i, i + 1);
		}
		assertThat(sizeCache()).isEqualTo(10);
		for (int i = 0; i < 30; i++) {
			if (i <= 19) {
				assertThat(getDigitoUnicoCache(this.digito, i)).isNull();
			} else {
				assertThat(getDigitoUnicoCache(this.digito, i)).isEqualTo((i + 1));
			}
		}
	}
}