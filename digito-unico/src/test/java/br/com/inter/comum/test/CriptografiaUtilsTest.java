package br.com.inter.comum.test;

import static br.com.inter.comum.CriptografiaUtils.criptografar;
import static br.com.inter.comum.CriptografiaUtils.descriptografar;
import static br.com.inter.comum.CriptografiaUtils.gerarChaves;
import static br.com.inter.comum.CriptografiaUtils.isChavesValida;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.comum.ChavesCriptografia;

@RunWith(NestedRunner.class)
@SpringBootTest
public class CriptografiaUtilsTest {

	private String textoBase = "Texto base para teste.";

	@Test
	public void criptografiar() {
		ChavesCriptografia chavesCriptografia = gerarChaves();
		byte[] textoCriptografado = criptografar(this.textoBase, chavesCriptografia.getChavePublica());
		String textoDescriptografado = descriptografar(textoCriptografado, chavesCriptografia.getChavePrivada());
		assertThat(this.textoBase).isEqualTo(textoDescriptografado);
	}

	@Test
	public void is_chaves_valida() {
		assertThat(isChavesValida("testeA", "testesB")).isFalse();
		assertThat(isChavesValida("", "")).isFalse();
		assertThat(isChavesValida(null, "testesB")).isFalse();
		assertThat(isChavesValida("testeA", null)).isFalse();
		ChavesCriptografia chavesCriptografia = gerarChaves();
		assertThat(isChavesValida(chavesCriptografia.getChavePublica(), chavesCriptografia.getChavePrivada())).isTrue();
	}
}