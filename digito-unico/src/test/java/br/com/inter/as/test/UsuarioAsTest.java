package br.com.inter.as.test;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.as.ConverterMapper;
import br.com.inter.as.UsuarioAS;
import br.com.inter.comum.ChavesCriptografia;
import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;
import br.com.inter.dto.DigitoUnicoDTO;
import br.com.inter.dto.UsuarioDTO;
import br.com.inter.service.UsuarioService;

@RunWith(NestedRunner.class)
@SpringBootTest
public class UsuarioAsTest {

	private static final int ID = 1;
	private UsuarioDTO usuarioDTO;
	private Usuario usuario;

	private UsuarioAS as;
	@Mock
	private UsuarioService service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.as = new UsuarioAS(this.service, new ConverterMapper());
	}

	public class FindAll extends UsuarioAsTest {

		@Override
		@Before
		public void setUp() {
			super.setUp();
			super.usuarioDTO = UsuarioDTO.builder().id(ID).build();
			super.usuario = Usuario.builder().id(ID).build();
			doAnswer(e -> asList(super.usuario)).when(super.service).findAll();
		}

		@Test
		public void chama_service() {
			super.as.findAll();
			verify(super.service).findAll();
		}

		@Test
		public void retorna_list() throws Exception {
			assertThat(super.as.findAll()).isEqualTo(asList(super.usuarioDTO));
		}
	}

	public class FindById extends UsuarioAsTest {

		@Override
		@Before
		public void setUp() {
			super.setUp();
			super.usuarioDTO = UsuarioDTO.builder().id(ID).build();
			super.usuario = Usuario.builder().id(ID).build();
			doAnswer(e -> super.usuario).when(super.service).findById(ID);
		}

		@Test
		public void chama_service() {
			super.as.findById(ID);
			verify(super.service).findById(ID);
		}

		@Test
		public void retorna_usuario() throws Exception {
			assertThat(super.as.findById(ID)).isEqualTo(super.usuarioDTO);
		}
	}

	public class DeleteById extends UsuarioAsTest {

		@Test
		public void chama_service() {
			super.as.deleteById(ID);
			verify(super.service).deleteById(ID);
		}
	}

	public class Save extends UsuarioAsTest {

		@Captor
		private ArgumentCaptor<Usuario> usuarioCaptor;

		@Override
		public void setUp() {
			super.setUp();
			super.usuarioDTO = UsuarioDTO.builder().nome("Name").email("nome@email.com").build();
		}

		@Test
		public void salvar_novo() {
			super.as.save(super.usuarioDTO);
			verify(super.service).save(this.usuarioCaptor.capture());
			assertThat(this.usuarioCaptor.getValue().getId()).isNull();
			assertThat(this.usuarioCaptor.getValue().getNome()).isEqualTo(super.usuarioDTO.getNome());
			assertThat(this.usuarioCaptor.getValue().getEmail()).isEqualTo(super.usuarioDTO.getEmail());
		}

		@Test
		public void alterar() {
			super.usuarioDTO.setId(ID);
			super.as.save(super.usuarioDTO);
			verify(super.service).save(this.usuarioCaptor.capture());
			assertThat(this.usuarioCaptor.getValue().getId()).isEqualTo(ID);
			assertThat(this.usuarioCaptor.getValue().getNome()).isEqualTo(super.usuarioDTO.getNome());
			assertThat(this.usuarioCaptor.getValue().getEmail()).isEqualTo(super.usuarioDTO.getEmail());
		}
	}

	public class Update extends UsuarioAsTest {

		@Captor
		private ArgumentCaptor<Usuario> usuarioCaptor;

		@Override
		public void setUp() {
			super.setUp();
			super.usuarioDTO = UsuarioDTO.builder().id(ID).nome("Nome").email("nome@email.com").build();
			doAnswer(e -> true).when(super.service).existUsuario(2);
		}

		@Test
		public void retorna_erro_ao_validar() {
			assertThat(catchThrowable(() -> super.as.update(null, super.usuarioDTO))).hasMessageContaining("ID usuário não pode ser nulo.");
			assertThat(catchThrowable(() -> super.as.update(ID, super.usuarioDTO))).hasMessageContaining("Usuário não existe.");
		}

		@Test
		public void alterar() {
			super.as.update(2, super.usuarioDTO);
			verify(super.service).save(this.usuarioCaptor.capture());
			assertThat(this.usuarioCaptor.getValue().getId()).isEqualTo(2);
			assertThat(this.usuarioCaptor.getValue().getNome()).isEqualTo(super.usuarioDTO.getNome());
			assertThat(this.usuarioCaptor.getValue().getEmail()).isEqualTo(super.usuarioDTO.getEmail());
		}
	}

	public class FindAllDigitoUnico extends UsuarioAsTest {

		private DigitoUnico digitoUnico;
		private DigitoUnicoDTO digitoUnicoDTO;

		@Override
		@Before
		public void setUp() {
			super.setUp();
			this.digitoUnico = DigitoUnico.builder().id(ID).build();
			this.digitoUnicoDTO = DigitoUnicoDTO.builder().id(ID).build();
			doAnswer(e -> asList(this.digitoUnico)).when(super.service).findAllDigitoUnico(ID);
		}

		@Test
		public void chama_service() {
			super.as.findAllDigitoUnico(ID);
			verify(super.service).findAllDigitoUnico(ID);
		}

		@Test
		public void retorna_list() throws Exception {
			assertThat(super.as.findAllDigitoUnico(ID)).isEqualTo(asList(this.digitoUnicoDTO));
		}
	}

	public class GerarChaves extends UsuarioAsTest {

		@Test
		public void gerar_chaves() {
			ChavesCriptografia chavesCriptografia = super.as.gerarChaves();
			assertThat(chavesCriptografia).isNotNull();
			assertThat(chavesCriptografia.getChavePublica()).isNotEmpty();
			assertThat(chavesCriptografia.getChavePrivada()).isNotEmpty();
		}
	}

	public class Criptografar extends UsuarioAsTest {

		@Test
		public void chama_service() {
			super.as.criptografar(ID, "chavePublica", "chavePrivada");
			verify(super.service).criptografar(ID, "chavePublica", "chavePrivada");
		}
	}

	public class Descriptografar extends UsuarioAsTest {

		@Test
		public void chama_service() {
			super.as.descriptografar(ID, "chavePrivada");
			verify(super.service).descriptografar(ID, "chavePrivada");
		}
	}
}