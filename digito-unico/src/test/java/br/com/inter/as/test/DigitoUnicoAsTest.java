package br.com.inter.as.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.as.DigitoUnicoAS;
import br.com.inter.dto.DigitoUnicoParamDTO;
import br.com.inter.service.DigitoUnicoService;

@RunWith(NestedRunner.class)
@SpringBootTest
public class DigitoUnicoAsTest {

	private static final int ID = 1;
	private static final Integer DIGITO = 9875;
	private static final Integer QTD_CONCATENACAO = 1;
	private DigitoUnicoAS as;

	@Mock
	private DigitoUnicoService service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.as = new DigitoUnicoAS(this.service);
	}

	public class Calcular extends DigitoUnicoAsTest {

		private DigitoUnicoParamDTO digitoUnicoParam;

		@Override
		public void setUp() {
			super.setUp();
			this.digitoUnicoParam = DigitoUnicoParamDTO.builder().idUsuario(ID).digito(DIGITO).qtdConcatenacao(QTD_CONCATENACAO).build();
			doAnswer(e -> 2).when(super.service).calcular(ID, DIGITO);
			doAnswer(e -> 2).when(super.service).calcular(ID, DIGITO.toString(), QTD_CONCATENACAO);
		}

		@Test
		public void validacao() {
			assertThat(catchThrowable(() -> super.as.calcular(null))).hasMessageContaining("Parametros não podem ser nulos.");
			DigitoUnicoParamDTO digitoUnicoParam = DigitoUnicoParamDTO.builder().build();
			assertThat(catchThrowable(() -> super.as.calcular(digitoUnicoParam))).hasMessageContaining("Digito não pode ser nulo.");
		}

		@Test
		public void calcular_digito() {
			assertThat(super.as.calcular(this.digitoUnicoParam)).isEqualTo(2);
			verify(super.service).calcular(ID, DIGITO.toString(), QTD_CONCATENACAO);

		}

		@Test
		public void calcular_digito_sem_concatenacao() {
			this.digitoUnicoParam.setQtdConcatenacao(null);
			assertThat(super.as.calcular(this.digitoUnicoParam)).isEqualTo(2);
			verify(super.service).calcular(ID, DIGITO);
		}
	}
}