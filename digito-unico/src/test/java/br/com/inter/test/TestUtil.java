package br.com.inter.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil {

	static public byte[] toJSONAsByte(Object... o) throws JsonProcessingException {
		return toJSONAsByte((Object) o);
	}

	static public byte[] toJSONAsByte(Object o) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		byte[] bytes = mapper.writeValueAsBytes(o);
		return bytes;
	}
}