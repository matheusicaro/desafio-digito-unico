package br.com.inter.test;

import java.nio.file.AccessDeniedException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.inter.comum.ServiceException;

@ControllerAdvice
public class APIGlobalExceptionHandler {
	private final Logger log = LoggerFactory.getLogger(APIGlobalExceptionHandler.class);

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler(value = AccessDeniedException.class)
	@ResponseBody
	public Map<String, Object> acessoNegado(HttpServletRequest req) {
		Map<String, Object> map = new HashMap<>();
		map.put("message", "Acesso negado.");
		map.put("url", req.getRequestURL());
		return map;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = RuntimeException.class)
	@ResponseBody
	public Map<String, Object> defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
			throw e;
		if (!(e instanceof ServiceException)) {
			log.warn("Tratando erro antes de enviar para o client", e);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("exception", e.getClass().getSimpleName());
		map.put("cause", ExceptionUtils.getRootCause(e));
		map.put("message", e.getMessage());
		map.put("url", req.getRequestURL());
		return map;
	}
}