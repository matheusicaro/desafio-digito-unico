package br.com.inter.api.test;

import static br.com.inter.test.TestUtil.toJSONAsByte;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.h2.server.web.WebApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.api.DigitoUnicoAPI;
import br.com.inter.as.DigitoUnicoAS;
import br.com.inter.dto.DigitoUnicoParamDTO;
import br.com.inter.test.APIGlobalExceptionHandler;

@RunWith(NestedRunner.class)
public class DigitoUnicoAPITest {

	private static final int ID = 1;
	private static final Integer DIGITO = 9875;
	private static final Integer QTD_CONCATENACAO = 1;
	private static final String URL = "/digito-unico";
	private MockMvc mockMvc;

	@Mock
	private DigitoUnicoAS as;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(new DigitoUnicoAPI(this.as)).setControllerAdvice(new APIGlobalExceptionHandler()).build();
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class Save extends DigitoUnicoAPITest {

		private DigitoUnicoParamDTO digitoUnicoParamDTO;
		private MockHttpServletRequestBuilder save;

		@Captor
		private ArgumentCaptor<DigitoUnicoParamDTO> captor;

		@Override
		@Before
		public void setup() {
			super.setup();
			this.digitoUnicoParamDTO = DigitoUnicoParamDTO.builder().idUsuario(ID).digito(DIGITO).qtdConcatenacao(QTD_CONCATENACAO).build();
			this.save = post(URL + "/calcular").contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.save.content(toJSONAsByte(this.digitoUnicoParamDTO))).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.save.content(toJSONAsByte(this.digitoUnicoParamDTO))).andExpect(status().isOk());
			verify(super.as).calcular(this.captor.capture());
			assertThat(this.captor.getValue().getIdUsuario()).isEqualTo(ID);
			assertThat(this.captor.getValue().getDigito()).isEqualTo(DIGITO);
			assertThat(this.captor.getValue().getQtdConcatenacao()).isEqualTo(QTD_CONCATENACAO);
		}
	}
}