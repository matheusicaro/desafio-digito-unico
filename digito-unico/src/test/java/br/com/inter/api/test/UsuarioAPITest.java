package br.com.inter.api.test;

import static br.com.inter.test.TestUtil.toJSONAsByte;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.h2.server.web.WebApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.nitorcreations.junit.runners.NestedRunner;

import br.com.inter.api.UsuarioAPI;
import br.com.inter.as.UsuarioAS;
import br.com.inter.comum.ChavesCriptografia;
import br.com.inter.dto.DigitoUnicoDTO;
import br.com.inter.dto.UsuarioDTO;
import br.com.inter.test.APIGlobalExceptionHandler;

@RunWith(NestedRunner.class)
public class UsuarioAPITest {

	private static final int ID = 1;
	private static final String URL = "/usuario";
	private MockMvc mockMvc;
	private UsuarioDTO usuarioDTO;

	@Mock
	private UsuarioAS as;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(new UsuarioAPI(this.as)).setControllerAdvice(new APIGlobalExceptionHandler()).build();
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class FindAll extends UsuarioAPITest {

		private MockHttpServletRequestBuilder findAll;

		@Override
		public void setup() {
			super.setup();
			this.findAll = get(URL).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.findAll).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.findAll).andExpect(status().isOk());
			verify(super.as).findAll();
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class FindById extends UsuarioAPITest {

		private MockHttpServletRequestBuilder findById;

		@Override
		public void setup() {
			super.setup();
			this.findById = get(URL + "/{id}", ID).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.findById).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.findById).andExpect(status().isOk());
			verify(super.as).findById(ID);
		}

		@Test
		public void valida_retorno_dto() throws Exception {
			DigitoUnicoDTO digitoUnicosDTO = DigitoUnicoDTO.builder().id(ID).digito("1").qtdConcatenacao(1).digitoUnico(1).build();
			UsuarioDTO usuarioDTO = UsuarioDTO.builder().id(ID).nome(EMPTY).email(EMPTY).digitoUnicos(asList(digitoUnicosDTO)).build();
			doAnswer(e -> usuarioDTO).when(super.as).findById(ID);
			ResultActions actions = super.mockMvc.perform(this.findById);
			actions.andExpect(status().isOk());
			actions.andExpect(jsonPath("$.id", is(ID)));
			actions.andExpect(jsonPath("$.nome", notNullValue()));
			actions.andExpect(jsonPath("$.email", notNullValue()));
			actions.andExpect(jsonPath("$.digitoUnicos", notNullValue()));
			actions.andExpect(jsonPath("$.digitoUnicos[0].id", notNullValue()));
			actions.andExpect(jsonPath("$.digitoUnicos[0].digito", notNullValue()));
			actions.andExpect(jsonPath("$.digitoUnicos[0].qtdConcatenacao", notNullValue()));
			actions.andExpect(jsonPath("$.digitoUnicos[0].digitoUnico", notNullValue()));
			actions.andExpect(jsonPath("$.*", hasSize(4)));
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	@SuppressWarnings("static-access")
	public class DeleteById extends UsuarioAPITest {

		private MockHttpServletRequestBuilder deleteById;

		@Before
		@Override
		public void setup() {
			super.setup();
			this.deleteById = delete(super.URL).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.deleteById.content(toJSONAsByte(ID))).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.deleteById.content(toJSONAsByte(ID))).andExpect(status().isOk());
			verify(super.as).deleteById(ID);
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class Update extends UsuarioAPITest {

		private MockHttpServletRequestBuilder update;

		@Captor
		private ArgumentCaptor<UsuarioDTO> captor;

		@Override
		@Before
		public void setup() {
			super.setup();
			super.usuarioDTO = UsuarioDTO.builder().id(ID).build();
			this.update = put(URL + "/{id}", ID).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.update.content(toJSONAsByte(super.usuarioDTO))).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.update.content(toJSONAsByte(super.usuarioDTO))).andExpect(status().isOk());
			verify(super.as).update(Mockito.eq(ID), this.captor.capture());
			assertThat(this.captor.getValue().getId()).isEqualTo(ID);
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class Save extends UsuarioAPITest {

		private MockHttpServletRequestBuilder save;

		@Captor
		private ArgumentCaptor<UsuarioDTO> captor;

		@Override
		@Before
		public void setup() {
			super.setup();
			super.usuarioDTO = UsuarioDTO.builder().id(ID).build();
			this.save = post(URL).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.save.content(toJSONAsByte(super.usuarioDTO))).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.save.content(toJSONAsByte(super.usuarioDTO))).andExpect(status().isOk());
			verify(super.as).save(this.captor.capture());
			assertThat(this.captor.getValue().getId()).isEqualTo(ID);
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class FindAllDigitoUnico extends UsuarioAPITest {

		private MockHttpServletRequestBuilder findAllDigitoUnico;

		@Override
		public void setup() {
			super.setup();
			this.findAllDigitoUnico = get(URL + "/" + ID + "/find-all-digito-unico").contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.findAllDigitoUnico).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.findAllDigitoUnico).andExpect(status().isOk());
			verify(super.as).findAllDigitoUnico(ID);
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class GerarChaves extends UsuarioAPITest {

		private MockHttpServletRequestBuilder gerarChaves;

		@Override
		public void setup() {
			super.setup();
			this.gerarChaves = get(URL + "/gerar-chaves-criptografia").contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.gerarChaves).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.gerarChaves).andExpect(status().isOk());
			verify(super.as).gerarChaves();
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class Criptografar extends UsuarioAPITest {

		private MockHttpServletRequestBuilder criptografar;
		private ChavesCriptografia chavesCriptografia;

		@Override
		@Before
		public void setup() {
			super.setup();
			this.chavesCriptografia = ChavesCriptografia.builder().chavePublica("chavePublica").chavePrivada("chavePrivada").build();
			this.criptografar = post(URL + "/{id}/criptografar", ID).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.criptografar.content(toJSONAsByte(this.chavesCriptografia))).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.criptografar.content(toJSONAsByte(this.chavesCriptografia))).andExpect(status().isOk());
			verify(super.as).criptografar(ID, "chavePublica", "chavePrivada");
		}
	}

	@RunWith(SpringRunner.class)
	@WebAppConfiguration
	@ContextConfiguration(classes = WebApp.class)
	public class Descriptografar extends UsuarioAPITest {

		private MockHttpServletRequestBuilder descriptografar;
		private ChavesCriptografia chavesCriptografia;

		@Override
		public void setup() {
			super.setup();
			this.chavesCriptografia = ChavesCriptografia.builder().chavePublica("chavePublica").chavePrivada("chavePrivada").build();
			this.descriptografar = post(URL + "/{id}/descriptografar", ID).contentType(MediaType.APPLICATION_JSON_UTF8);
		}

		@Test
		public void retorna_status_200() throws Exception {
			super.mockMvc.perform(this.descriptografar.content(toJSONAsByte(this.chavesCriptografia))).andExpect(status().isOk());
		}

		@Test
		public void chama_AS() throws Exception {
			super.mockMvc.perform(this.descriptografar.content(toJSONAsByte(this.chavesCriptografia))).andExpect(status().isOk());
			verify(super.as).descriptografar(ID, this.chavesCriptografia.getChavePrivada());
		}
	}
}