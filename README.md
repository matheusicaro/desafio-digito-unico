# Banco Inter Teste

## Desenvolvimento

### Montagem do ambiente

Para a execução/desenvolvimento da aplicação é necessário a instalação dos seguintes programas em sua estação de trabalho:

1. Java 1.8+
1. Mavem 3+
2. H2

## Execução
OBS: O passo a passo da execução foi elaborado para SO *Windows*.

Certifique-se de abrir o CMD na raiz da aplicação.

### Testes

Para a execução dos testes execute o comando:

```sh
mvn test
```

### Run

Para a execução da aplicação execute o comando:

```sh
mvn spring-boot:run
```

*FIM*

*Desde já, agradeço a oportunidade pelo desenvolvimento desse teste.*

*Autor:* Elder Bugareli dos Reis
*e-mail:* elderbugareli@gmail.com